/** @type {import('next').NextConfig} */
const nextConfig = {
  output: "standalone",
  reactStrictMode: true,
  images: {
    domains: ["sangw.in", "localhost", "picsum.photos"], // <== Domain name
  },
};
module.exports = nextConfig;
