import { titleFont } from '@/config/fonts'
import Link from 'next/link'
import React from 'react'

interface Props {
    title: string,
    subtitle?: string,
    className?: string 
  }

export const Title = ({title, subtitle, className}: Props) => {
    return (
        <div className={`mt-3 ${className}`}>
            <div className='flex justify-between items-center'>
                <h1 className={`${titleFont.className} antialiased text-4xl font-semibold my-10`}>
                    {title}
                </h1>
                <Link
                    href={'/noticia/crear-noticia'}
                    className="mx-2">
                    Crear Noticia
                </Link>
            </div>
            {
                subtitle && (
                    <h3 className='text-xl mb-5'>
                        {subtitle}
                    </h3>
                )
            }
        </div>
  )
}
