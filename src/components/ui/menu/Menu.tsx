'use client';

import { titleFont } from "@/config/fonts"
import Link from "next/link"
import { FaRegUser } from "react-icons/fa";
import { Search } from "./Search";

export const Menu = () => {
    return (
        <nav className="flex px-5 justify-between items-center w-full">
            {/* Logo */}
            <div>
                <Link
                    href={"/"}>
                    <span className={`${titleFont.className} antialiased font-bold`}>El Hocicon</span>
                </Link>
            </div>

            {/* Search, Auth */}
            <div className="flex items-center m-2">
                <Search/>
                <Link
                    href={'/auth/login'}
                    className="mx-2">
                    <FaRegUser className="w-7 h-7"/>Ingresar
                </Link>
            </div>
        </nav>
    )
}
