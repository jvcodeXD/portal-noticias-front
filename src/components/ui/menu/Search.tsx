import Link from 'next/link'
import React, { ChangeEvent, useState } from 'react'
import { IoSearchOutline } from 'react-icons/io5'

export const Search = () => {
    const [term, setTerm] = useState('')
    const handleTerm = (e: ChangeEvent<HTMLInputElement>) => {
        setTerm(e.target.value)
    }
    return (
        <div
            className="mx-2">
            <div className="relative">
                <input type="text"
                    placeholder="Buscar"
                    onChange={handleTerm}
                    className="pl-4 pr-10 py-2 border rounded-full"/>
                <Link
                    href={`/category/${term}`}
                    className='btn-success'>
                    <IoSearchOutline
                        className="absolute right-3 top-1/2 transform -translate-y-1/2 w-7 h-7 text-gray-500" />
                </Link>
            </div>
        </div>
    )
}
