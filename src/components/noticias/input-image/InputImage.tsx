import { Imagen } from '@/interfaces';
import React, { ChangeEvent } from 'react'

interface Props {
    setImagenes: React.Dispatch<React.SetStateAction<Imagen[]>>
}
    
export const InputImage = ({setImagenes}: Props) => {

    const handleChangeImage = (e: ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files;
        if (files) {
          const newImages: Imagen[] = Array.from(files).map((archivo) => ({
            archivo,
            nombre: archivo.name,
          }));
    
          setImagenes(newImages);
        }
    };
    
  return (
    <div className="flex flex-col mb-2">
        <span>Imagen</span>
        <input
            type="file"
            multiple
            name='images'
            accept='image/*'
            className='mt-1 p-2 border rounded-md'
            onChange={handleChangeImage} />
      </div>
  )
}
