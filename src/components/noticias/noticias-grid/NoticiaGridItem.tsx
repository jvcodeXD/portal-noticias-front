'use client';

import { NoticiaRes } from '@/interfaces'
import Image from 'next/image'
import Link from 'next/link'
import React from 'react'

interface Props {
    noticia: NoticiaRes
}

export const NoticiaGridItem = ({ noticia }: Props) => {
    
    const fecha = new Date(noticia.date).toLocaleString()

    return (
        <div className='rounded-md overflow-hidden fade-in p-5'>
            <div className='relative'>
                <Link href={`/noticia/${noticia.title}`}>
                    <Image
                        src={`http://localhost:4000/api/files/noticias/${noticia.images[0]}`}
                        alt={noticia.title}
                        width={100}
                        height={100}
                        className='rounded-lg h-full w-full object-fill'
                    />
                </Link>
                <small>
                    <span className="absolute top-0 right-0 p-1 rounded bg-gray-500 bg-opacity-50 text-white">
                        {`${fecha}`}
                    </span>
                </small>
            </div>
            <div className='p-4 flex flex-col'>
                <Link
                    className='hover:text-blue-500'
                    href={`/noticia/${noticia.title}`}>
                    <span className='font-bold text-2xl'>{noticia.title}</span>
                </Link >
                <span>Autor: <i>{noticia.author}</i></span>
            </div>
        </div>
    )
}
