import { NoticiaRes } from '@/interfaces'
import React from 'react'
import { NoticiaGridItem } from './NoticiaGridItem'

interface Props {
    noticias: NoticiaRes[]
}

export const NoticiaGrid = ({noticias}: Props) => {
  
    return (
        <div className='grid grid-cols-2 mb-10'>
            {
                noticias.map(noticia => (
                    <NoticiaGridItem
                        key={noticia.id}
                        noticia={noticia}
                    />
                ))
            }
        </div>
    )
}
