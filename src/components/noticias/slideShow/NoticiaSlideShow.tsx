'use client';

import React, { useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import {Swiper as SwiperObject} from 'swiper'

import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';

import './slideShow.css';
import Image from 'next/image';

import { Autoplay, FreeMode, Navigation, Thumbs } from 'swiper/modules';

interface Props {
  images: string[],
  title: string,
  className?: string
}

export const NoticiaSlideShow = ({ images, title, className }: Props) => {
  
    const [thumbsSwiper, setThumbsSwiper] = useState<SwiperObject>();

    return (
        <div className={ className }>
            <Swiper
                style={{
                    '--swiper-navigation-color': '#fff',
                    '--swiper-pagination-color': '#fff',
                } as React.CSSProperties
                }
                loop={true}
                spaceBetween={10}
                navigation={true}
                autoplay= {{ delay: 2500 }}
                thumbs={{
                    swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null
                }}
                modules={[FreeMode, Navigation, Thumbs, Autoplay]}
                className="mySwiper2"
            >
                {
                    images && images.map((image, index) => (
                        <SwiperSlide
                        key={index}>
                            <Image
                                width={ 1024 }
                                height={800}
                                src={`http://localhost:4000/api/files/noticias/${image}`}
                                alt={ image }
                                className="rounded-lg object-fill"/>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
            <Swiper
                onSwiper={setThumbsSwiper}
                loop={true}
                spaceBetween={10}
                slidesPerView={5}
                freeMode={true}
                autoplay= {{ delay: 2500 }}
                watchSlidesProgress={true}
                modules={[FreeMode, Navigation, Thumbs, Autoplay]}
                className="mySwiper"
            >
                {
                    images && images.map((image, index) => (
                        <SwiperSlide
                            key={index}>
                            <Image
                                width={ 300 }
                                height={300}
                                src={`http://localhost:4000/api/files/noticias/${image}`}
                                alt={ image }
                                className="rounded-lg object-fill"/>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    )
}
