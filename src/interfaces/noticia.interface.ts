export interface Noticia {
    id?: string,
    title: string,
    images: string[],
    date?: Date,
    place: string, 
    author: string,
    content: string
}

export interface NoticiaRes {
    id: string,
    title: string,
    images: string[],
    date: Date,
    place: string, 
    author: string,
    content: string
}