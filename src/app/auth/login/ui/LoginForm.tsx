"use client";

import { ChangeEvent, useState } from 'react';
import { User } from '@/interfaces';
import axios from 'axios';

export const LoginForm = () => {

    const [user, setUser] = useState<User>({
        email: '',
        password: '',
    })

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target
        setUser({ ...user, [name]: value })
    }

    const logIn = async (e: React.FormEvent) => {
        e.preventDefault()
        await axios.post(`http://localhost:4000/api/auth/login`, user)
            .then(res => {
                localStorage.setItem('user', res.data)
                window.location.replace('/')
            })
        .catch(err => console.log(err))
    }

    return (
        <form action='' className="flex flex-col">
            <label htmlFor="email">Correo electrónico</label>
            <input
                onChange={handleChange}
                className="px-5 py-2 border bg-gray-200 rounded mb-5"
                type="email"
                name="email"
            />

            <label htmlFor="email">Contraseña</label>
            <input
                onChange={handleChange}
                className="px-5 py-2 border bg-gray-200 rounded mb-5"
                type="password"
                name="password"
            />

            <button 
                type="submit"
                onClick={logIn}  
                className='btn-primary'
            >
                Ingresar
            </button>
        </form>
    );
};
