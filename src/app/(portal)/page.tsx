'use client';

import { NoticiaGrid, Title } from "@/components";
import { NoticiaRes } from "@/interfaces";

import axios from "axios";
import { useEffect, useState } from "react";

const Page = () => {
  
  const [noticias, setNoticias] = useState<NoticiaRes[]>([])

    const getNoticias = async () => {
        await axios.get(`http://localhost:4000/api/noticias`)
            .then(res => {
                setNoticias(res.data);
            })
            .catch(err => console.log(err))
    }
    
    useEffect(() => {
        getNoticias()
    }, [])
  
  return (
    <>
        <Title
            title='Portal de Noticias'
            subtitle='Noticias destacadas'
            className='mb-2'
          />
        {noticias && <NoticiaGrid
            noticias={noticias}
        />}
    </>
  )
}
export default Page;