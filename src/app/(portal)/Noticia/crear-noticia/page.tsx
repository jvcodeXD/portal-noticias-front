'use client';

import { Title } from '@/components';
import { Imagen, Noticia } from '@/interfaces';
import { ChangeEvent, useState } from 'react';
import { InputImage } from '@/components/noticias/input-image/InputImage';

import axios from 'axios';

const Page = () => {
  const [imagenes, setImagenes] = useState<Imagen[]>([]);
  const [noticia, setNoticia] = useState<Noticia>({
    title: '',
    images: [],
    place: '', 
    author: '',
    content: ''
  })

  const handleChange = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.target
    setNoticia({ ...noticia, [name]: value })
  }

  const crearNoticia = async () => {
    await enviarDatos(imagenes)
      await axios.post(`http://localhost:4000/api/noticias/`,noticia)
          .then(res => {
              window.location.replace('/')
          })
          .catch(err => console.log(err))
  }

  const enviarDatos = async (imagenes: Imagen[]) => {
    try {
      const resultados = await Promise.all(
        imagenes.map(async (imagen, index) => {
          const formData = new FormData();
          formData.append('file', imagen.archivo);
  
          const respuesta = await axios.post('http://localhost:4000/api/files/noticias/', formData);
          return { index, url: respuesta.data.secureUrl };
        })
      );
      const nuevasURLs = resultados.map((resultado) => resultado.url);
      noticia.images = [...noticia.images, ...nuevasURLs];
    } catch (error) {
      console.error('Error al enviar los datos:', error);
    }
  };

  return (
    <div className="flex flex-col sm:justify-center sm:items-center mb-72 px-10 sm:px-0">
      <div className="w-full  xl:w-[1000px] flex flex-col justify-center text-left">
        <Title title="Crear Noticia" subtitle="" />
        <div className="grid grid-cols-1 gap-2 sm:gap-5 sm:grid-cols-2">
            <div className="flex flex-col mb-2 sm:col-span-2">
              <span>Titulo</span>
              <input 
                type="text" 
                name='title'
                onChange={handleChange}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <div className="flex flex-col mb-2">
              <span>Lugar</span>
              <input 
                type="text" 
                name='place'
                onChange={handleChange}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <div className="flex flex-col mb-2">
              <span>Author</span>
              <input 
                type="text"
                name='author'
                onChange={handleChange}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>
            
            <div className="flex flex-col mb-2 sm:col-span-2">
              <span>Contenido</span>
              <textarea
                name="content"
                onChange={handleChange}
                rows={8}
                cols={80}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <InputImage setImagenes={setImagenes}/>

            <div className="flex flex-col mb-2 sm:mt-10">
              <button 
                onClick={crearNoticia}
                className="btn-primary flex w-full sm:w-1/2 justify-center ">
                Siguiente
              </button>
            I</div>
          
        </div>
      </div>
    </div>
  );
}

export default Page;