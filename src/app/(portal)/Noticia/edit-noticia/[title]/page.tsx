'use client';

import { Title } from '@/components';
import { Imagen, Noticia, NoticiaRes } from '@/interfaces';
import { ChangeEvent, useEffect, useState } from 'react';
import { InputImage } from '@/components/noticias/input-image/InputImage';

import axios from 'axios';
import notFound from '../../not-found';

interface Props {
    params:{
        title: string
    }
}

const Page = ({ params }: Props) => {
    const {title}  = params
    const [noticia, setNoticia] = useState<NoticiaRes>({
        id: '',
        title: '',
        images: [],
        place: '', 
        author: '',
        content: '',
        date: new Date()
      })
    const [imagenes, setImagenes] = useState<Imagen[]>([]);
    const getNoticia = async () => {
        await axios.get(`http://localhost:4000/api/noticias/${title}`)
            .then(res => {
                setNoticia(res.data);
            })
            .catch(err => console.log(err))
    }
    
    useEffect(() => {
        getNoticia()
    }, [])

    if (!noticia)
        notFound()
        const handleChange = (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
          const { name, value } = e.target
          setNoticia({ ...noticia, [name]: value })
        }
      
        const updateNoticia = async () => {
          await enviarDatos(imagenes)
          const { id, title, images, author, place, content }: Noticia = noticia
          const newNoticia: Noticia = {title, images, author, place, content}
          await axios.patch(`http://localhost:4000/api/noticias/${id}`,newNoticia)
              .then(res => {
                  window.location.replace(`/noticia/${res.data.title}`)
              })
              .catch(err => console.log(err))
        }
      
        const enviarDatos = async (imagenes: Imagen[]) => {
            try {
              const resultados = await Promise.all(
                imagenes.map(async (imagen, index) => {
                  const formData = new FormData();
                  formData.append('file', imagen.archivo);
          
                  const respuesta = await axios.post('http://localhost:4000/api/files/noticias/', formData);
                  return { index, url: respuesta.data.secureUrl };
                })
              );
              const nuevasURLs = resultados.map((resultado) => resultado.url);
              noticia.images = [...noticia.images, ...nuevasURLs];
            } catch (error) {
              console.error('Error al enviar los datos:', error);
            }
        };
  return (
    <div className="flex flex-col sm:justify-center sm:items-center mb-72 px-10 sm:px-0">
      <div className="w-full  xl:w-[1000px] flex flex-col justify-center text-left">
        <Title title="update Noticia" subtitle="" />
        <div className="grid grid-cols-1 gap-2 sm:gap-5 sm:grid-cols-2">
            <div className="flex flex-col mb-2 sm:col-span-2">
              <span>Titulo</span>
              <input 
                type="text" 
                name='title'
                onChange={handleChange}
                value={noticia?.title}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <div className="flex flex-col mb-2">
              <span>Lugar</span>
              <input 
                type="text" 
                name='place'
                onChange={handleChange}
                value={noticia?.place}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <div className="flex flex-col mb-2">
              <span>Author</span>
              <input 
                type="text"
                name='author'
                onChange={handleChange}
                value={noticia?.author}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>
            
            <div className="flex flex-col mb-2 sm:col-span-2">
              <span>Contenido</span>
              <textarea
                name="content"
                onChange={handleChange}
                rows={8}
                cols={80}
                value={noticia?.content}
                className="p-2 border rounded-md bg-gray-200"
              />
            </div>

            <InputImage setImagenes={setImagenes}/>

            <div className="flex flex-col mb-2 sm:mt-10">
              <button 
                onClick={updateNoticia}
                className="btn-primary flex w-full sm:w-1/2 justify-center ">
                Editar
              </button>
            I</div>
          
        </div>
      </div>
    </div>
  );
}

export default Page