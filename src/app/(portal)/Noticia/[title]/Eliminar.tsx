import axios from 'axios';
import React, { useState } from 'react'
import { AiOutlineDelete } from 'react-icons/ai'

interface Props {
    id: string
}

export const Eliminar = ({id}: Props) => {

    const [showModal, setShowModal] = useState(false);

    const handleEliminarClick = () => {
        setShowModal(!showModal);
    };

    const confirmation = async() => {
        setShowModal(!showModal);
        await axios.delete(`http://localhost:4000/api/noticias/${id}`)
            .then(res => window.location.replace('/'))
            .catch(err=>console.log(err))
    };

    return (
        <>
            <button
                onClick={handleEliminarClick}
                className="flex items-center text-red-500 hover:text-red-700"
                >
                <AiOutlineDelete className="mr-2" />
                Eliminar
            </button>
            
            {showModal && (
                <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-75">
                    <div className="bg-white p-4 rounded-md">
                        <p>¿Estás seguro de que quieres eliminar?</p>
                        <button onClick={confirmation} className="bg-red-500 text-white px-4 py-2 rounded-md mr-2">
                        Confirmar
                        </button>
                        <button onClick={handleEliminarClick} className="bg-gray-300 px-4 py-2 rounded-md">
                        Cancelar
                        </button>
                    </div>
                </div>
            )}
            </>
  )
}
