import React from 'react'
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai'
import { Eliminar } from './Eliminar'
import Link from 'next/link'

interface Props {
  id: string,
  title: string
}

export const Options = ({id, title}: Props) => {
  return (
    <>
      <div className="flex items-center space-x-4 p-4 border border-gray-300 rounded-md">
        <Link
            href={`/noticia/edit-noticia/${title}`}
            className="flex items-center text-yellow-500 hover:text-yellow-700">
          <AiOutlineEdit className="mr-2" />
          Editar
        </Link>
        <Eliminar id={id} />
    </div>
    </>
  )
}
