'use client'

import React, { useEffect, useState } from 'react'
import notFound from '../not-found'
import { titleFont } from '@/config/fonts'
import { NoticiaSlideShow } from '@/components'
import { NoticiaRes } from '@/interfaces'
import axios from 'axios'
import Link from 'next/link'
import { Options } from './Options'

interface Props {
    params: {
        title: string
    }
}

const Page = ({ params }: Props) => {
    const {title}  = params
    const [noticia, setNoticia] = useState<NoticiaRes>()
    const getNoticia = async () => {
        await axios.get(`http://localhost:4000/api/noticias/${title}`)
            .then(res => {
                setNoticia(res.data);
            })
            .catch(err => console.log(err))
    }
    
    useEffect(() => {
        getNoticia()
    }, [])

    if (!noticia)
        notFound()
    
    return (
        <div className='mt-5 mb-20 grid grid-cols-1 md:grid-cols-3 gap-3'>
            <div className='col-span-1 md:col-span-2 relative'>
                {
                    noticia && <NoticiaSlideShow
                    title={noticia.title}
                    images={noticia.images}
                    className='hidden md:block'
                />
                }
                <div className="absolute top-0 right-0 p-4 z-10">
                    <Options
                        id={noticia ? noticia.id : ''}
                        title={ noticia ? noticia.title: '' } />
          </div>
            </div>
            {/* Detalles */}
            <div className="col-span-1 px-5">
                <div>
                    <h1 className={`${titleFont.className} antialiased font-bold text-xl`}>
                        {noticia?.title}
                    </h1>
                </div>
                <small>
                    {noticia?.place} - <small className='text-xs bg-orange-200'>{ `${noticia?.date?new Date(noticia.date).toLocaleString():'fecha'}` }</small>
                </small>
                <p>
                    <strong>Noticia: </strong>
                    {noticia?.content}.
                    (<small>{ noticia?.author }</small>)
                </p>
                
                <Link
                    className='border rounded shadow-lg bg-blue-200'
                    href="/">
                    Regresar
                </Link>
            </div>
        </div>
    )
}
export default Page