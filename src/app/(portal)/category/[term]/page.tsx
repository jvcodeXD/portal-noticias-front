'use client';

import { NoticiaGrid, Title } from '@/components';
import { NoticiaRes } from '@/interfaces';
import axios from 'axios';
import { notFound } from 'next/navigation';
import { useEffect, useState } from 'react';



interface Props {
  params: {
    term: string;
  }
}


const Page = ({ params }: Props) => {

  const { term } = params;
  const [noticias, setNoticias] = useState<NoticiaRes[]>([])

    const getNoticias = async () => {
        await axios.get(`http://localhost:4000/api/noticias/category/${term}`)
            .then(res => {
                setNoticias(res.data);
            })
            .catch(err => console.log(err))
    }
    
    useEffect(() => {
        getNoticias()
    }, [])

  return (
    <>
      <Title
            title={`Portal de Noticias`}
            subtitle={`Noticias destacadas de ${term}`}
            className='mb-2'
        />
        <NoticiaGrid
            noticias={noticias}
        />
    </>
  );
}

export default Page