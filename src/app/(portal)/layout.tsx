import { Menu } from "@/components";

export default function PortalLayout({children}: {
        children: React.ReactNode;
    }) {
    return (
        <main className="min-h-screen">
            
            <Menu />
            
            <div className="px-5">
                {children}
            </div>
        </main>
    )
}