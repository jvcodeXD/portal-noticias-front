<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://seeklogo.com/images/N/next-js-logo-7929BCD36F-seeklogo.com.png" width="200" alt="Next Logo" /></a>
</p>

# Ejecutar en desarrollo

1. Clonar el repositorio
2. Instalar paquetes

```
npm install
```

3. Ejecutar

```
npm run dev
```

## Stack usado

- NextJS

# Ejecutar en desarrollo

- docker build -t nextjs-portal-noticias .

- docker container run -dp 3000:3000 nextjs-portal-noticias
